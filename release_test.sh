#!/bin/sh

VERSION=$1
DIST_DIR='/tmp/aiomas-dist'
WHL_DIR='/tmp/aiomas-wheel'
PYTHON=`which python3.6`
TESTCMD='import aiomas; print(aiomas.__version__)'

BOLD='\033[1;37m'
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'  # No Color

echo "${BOLD}Checking packages for aiomas==$VERSION ...${NC}"

run_test () {
    echo "${BOLD}Testing ${TYPE} package ...${NC}"
    rm -rf $DIR  # ensure clean state if ran repeatedly
    virtualenv -p $PYTHON $DIR
    ${DIR}/bin/pip install dist/${FNAME}[mpb]
    OUTPUT=`${DIR}/bin/python -c "${TESTCMD}"`
    if [[ "$OUTPUT" != "$VERSION" ]]; then
        echo "${RED}Expected version ${VERSION} but got ${OUTPUT}{$NC}"
        exit 2
    else
        echo "${GREEN}${TYPE} package looks okay.${NC}"
    fi
}

TYPE='Source'
DIR=$DIST_DIR
FNAME="aiomas-${VERSION}.tar.gz"
run_test

TYPE='Wheel'
DIR=$WHL_DIR
FNAME="aiomas-${VERSION}-py2.py3-none-any.whl"
run_test
