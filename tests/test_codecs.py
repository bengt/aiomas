import pytest

from aiomas.exceptions import SerializationError
import aiomas.codecs


@aiomas.codecs.serializable
class Spam:
    def __init__(self, a, b):
        self.a = a
        self._b = b

    @property
    def b(self):
        return self._b

    def __eq__(self, other):
        return (self.a, self.b) == (other.a, other.b)


def encode_spam(spam):
    return (spam.a, spam.b)


def decode_spam(spam_tuple):
    return Spam(*spam_tuple)


@pytest.fixture(params=[
    aiomas.codecs.JSON,
    aiomas.codecs.MsgPack,
    aiomas.codecs.MsgPackBlosc,
])
def codec(request):
    return request.param()


def test_encode_decode(codec):
    msg = ('ohai', {'spam': 'eggs'})
    encoded = codec.encode(msg)
    assert type(encoded) is bytes
    decoded = codec.decode(encoded)
    assert tuple(decoded) == msg


def test_add_serializer(codec):
    codec.add_serializer(Spam, encode_spam, decode_spam)
    msg = ('spam', Spam(1, 2))
    encoded = codec.encode(msg)
    decoded = codec.decode(encoded)
    assert tuple(decoded) == msg
    assert str(codec) == '{}[Spam]'.format(codec.__class__.__name__)


def test_add_serializer_twice(codec):
    """An exception should be raised if another serializer is added for the
    same type."""
    codec.add_serializer(Spam, encode_spam, decode_spam)
    pytest.raises(ValueError, codec.add_serializer,
                  Spam, encode_spam, decode_spam)


def test_add_generic_serializer(codec):
    codec.add_serializer(object, encode_spam, decode_spam)
    msg = ('spam', Spam(1, 2))
    encoded = codec.encode(msg)
    decoded = codec.decode(encoded)
    assert tuple(decoded) == msg


def test_no_serializer_found(codec):
    msg = ('spam', Spam(1, 2))
    pytest.raises(SerializationError, codec.encode, msg)


def test_serializable(codec):
    codec.add_serializer(*Spam.__serializer__())
    expected = Spam(1, 2)
    result = codec.decode(codec.encode(expected))
    assert result == expected
    assert repr(result) == 'Spam(a=1, b=2)'
    assert str(result) == repr(result)


def test_serializable_no_repr():
    @aiomas.codecs.serializable(repr=False)
    class A:
        def __init__(self, a):
            self.a = a

    assert A.__repr__.__class__ is not A
    assert not repr(A(3)).startswith('A(')
    assert A(3).__asdict__() == {'a': 3}
