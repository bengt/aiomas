if __name__ == '__main__':
    import sys

    import aiomas
    import aiomas.subproc
    import arrow

    clock = aiomas.ExternalClock(arrow.get(sys.argv[1]))
    task = aiomas.subproc.start(('127.0.0.1', 5556), clock=clock)
    aiomas.run(until=task)
