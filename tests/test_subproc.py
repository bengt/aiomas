import asyncio
import os
import platform
import signal
import subprocess
import sys

from aiomas import agent
import aiomas
import arrow
import pytest


ON_WINDOWS = platform.system() == 'Windows'


class MyAgent(aiomas.Agent):
    def __init__(self, container, name):
        super().__init__(container)
        self._name = name

    @aiomas.expose
    def method(self, array):
        return (self._name, array)

    @aiomas.expose
    def get_time(self):
        return self.container.clock.time()

    @aiomas.expose
    def raise_kb(self):
        raise KeyboardInterrupt


@pytest.fixture
def start():
    return arrow.get()


@pytest.fixture
def subproc_container(start):
    curdir = os.path.dirname(__file__)

    cmd = [
        sys.executable,
        os.path.join(curdir, 'run_testagent.py'),
        str(start),
    ]

    # Update ENV with PYTHONPATH so that the remote container can find and
    # import our MyAgent class:
    env = os.environ.copy()
    pp = env.get('PYTHONPATH', '')
    if pp:
        pp += ':'
    env['PYTHONPATH'] = pp + curdir

    proc = subprocess.Popen(cmd, env=env, universal_newlines=True)
    yield proc
    if proc.poll() is None:
        proc.terminate()
        proc.wait(0.1)


@pytest.fixture
def container(event_loop):
    c = agent.Container.create(('127.0.0.1', 5555), loop=event_loop)
    yield c
    c.shutdown()


@pytest.mark.asyncio
async def test_container(subproc_container, container, start):
    mgr = await container.connect('tcp://127.0.0.1:5556/0', timeout=1)

    ta, ta_addr = await mgr.spawn('{}:MyAgent'.format(__name__), 'spam')
    assert str(ta) == "Proxy(('127.0.0.1', 5556), 'agents/1')"
    assert ta_addr == 'tcp://127.0.0.1:5556/1'

    # Test if the agent received its named and the serializers were set up:
    ret = await ta.method([0, 1, 2])
    assert ret == ['spam', [0, 1, 2]]

    # Test setting the clock
    t = await ta.get_time()
    assert t == 0
    await mgr.set_time(42)
    t = await ta.get_time()
    assert t == 42

    await mgr.stop()


@pytest.mark.asyncio
async def test_container_kb_interupt(subproc_container, event_loop):
    if ON_WINDOWS:
        pytest.skip('Signals suck on Windows')
    await asyncio.sleep(1, loop=event_loop)
    subproc_container.send_signal(signal.SIGINT)
    subproc_container.wait(timeout=0.1) == 1
