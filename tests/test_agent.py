import asyncio
import platform
import socket
import unittest.mock

import arrow
import pytest

from aiomas import expose, Container, Agent
import aiomas.agent
import aiomas.codecs
import aiomas.clocks
import aiomas.local_queue
import aiomas.rpc


ON_WINDOWS = platform.system() == 'Windows'


class ExampleAgent(Agent):
    async def run(self, target_addr, timeout=0):
        remote_agent = await self.container.connect(target_addr, timeout)
        assert type(remote_agent) is aiomas.rpc.Proxy
        assert str(remote_agent) == 'ExampleAgentProxy({!r})'.format(
            target_addr)

        num = 2
        res = await remote_agent.service(num)
        assert res == num

        await remote_agent.get_addr()

    @expose
    async def service(self, val):
        await asyncio.sleep(0.001, self.container.loop)
        return val

    @expose
    def get_addr(self):
        return self.addr


def test_str(event_loop):
    c = Container.create(('127.0.0.1', 5555), loop=event_loop)
    a = ExampleAgent(c)
    assert str(c) == "Container('tcp://127.0.0.1:5555/')"
    assert str(a) == "ExampleAgent('tcp://127.0.0.1:5555/0')"
    c.shutdown()


def test_agent_init_errors(event_loop):
    pytest.raises(TypeError, Agent, None)


@pytest.mark.parametrize(['bind', 'connect'], [
    (('127.0.0.1', 5555), ('127.0.0.1', 5555)),
    (('localhost', 5556), ('localhost', 5556)),
    (('', 5557), ('localhost', 5557)),
    (('0.0.0.0', 5558), ('localhost', 5558)),
    (('::', 5559), ('::1', 5559)),
])
def test_agents_single_container(event_loop, bind, connect):
    c = Container.create(bind, loop=event_loop)

    # Check if the base URL was set properly:
    if bind[0] in [None, '', '::', '0.0.0.0']:
        assert c._base_url == 'tcp://%s:%s/' % (socket.getfqdn(), bind[1])
    else:
        assert c._base_url == 'tcp://%s:%s/' % bind

    agents = [ExampleAgent(c) for i in range(2)]
    event_loop.run_until_complete(agents[1].run('tcp://%s:%d/0' % connect))
    c.shutdown()


@pytest.mark.parametrize(['bind', 'connect'], [
    ('127.0.0.1', '127.0.0.1'),
    ('localhost', 'localhost'),
    ('', 'localhost'),
    ('0.0.0.0', 'localhost'),
])
def test_agents_multi_container(event_loop, bind, connect):
    c0 = Container.create((bind, 5555), loop=event_loop)
    c1 = Container.create((bind, 5556), loop=event_loop)
    ExampleAgent(c0)
    a1 = ExampleAgent(c1)
    event_loop.run_until_complete(a1.run('tcp://%s:5555/0' % connect))
    c0.shutdown()
    c1.shutdown()


def test_agent_with_sub_and_default_router(event_loop):
    class ExampleAgent(Agent):
        def __init__(self, container):
            self.sub = Sub()
            self.router.add('sub')
            super().__init__(container)

        async def run(self, target_addr):
            remote_agent = await self.container.connect(target_addr)
            res = await remote_agent.sub.spam()
            assert res == 3

    class Sub:
        router = aiomas.rpc.Service()

        @router.expose
        def spam(self):
            return 3

    c = Container.create(('localhost', 5555), loop=event_loop)
    agents = [ExampleAgent(c) for i in range(2)]
    event_loop.run_until_complete(agents[1].run('tcp://localhost:5555/0'))
    c.shutdown()


def test_agent_with_sub_and_custom_router(event_loop):
    class ExampleAgent(Agent):
        router = aiomas.rpc.Service(['sub'])

        def __init__(self, container):
            self.sub = Sub()
            super().__init__(container)

        async def run(self, target_addr):
            remote_agent = await self.container.connect(target_addr)
            res = await remote_agent.sub.spam()
            assert res == 3

    class Sub:
        router = aiomas.rpc.Service()

        @router.expose
        def spam(self):
            return 3

    c = Container.create(('localhost', 5555), loop=event_loop)
    agents = [ExampleAgent(c) for i in range(2)]
    event_loop.run_until_complete(agents[1].run(agents[0].addr))
    c.shutdown()


@pytest.mark.skipif('ON_WINDOWS', reason='Not implemented on Windows')
def test_container_unix_domain_sockets(event_loop, short_tmpdir):
    """Containers should also work with unix domain sockets."""
    addr = short_tmpdir.join('s').strpath
    c = Container.create(addr, loop=event_loop)
    a = aiomas.Agent(c)
    assert a.addr == 'ipc://[%s]/0' % addr

    async def client():
        con = await c.connect(a.addr)
        assert type(con) is aiomas.rpc.Proxy
        # Give the service the chance to start
        await asyncio.sleep(0.01, loop=event_loop)

    event_loop.run_until_complete(client())
    c.shutdown()


def test_connection_cache(event_loop):
    """Test that the connection cache is hit from the second connect."""
    class ExampleAgent(Agent):
        async def run(self, target_addr):
            remote_agent = await self.container.connect(target_addr)
            assert type(remote_agent) is aiomas.rpc.Proxy

    with unittest.mock.patch('aiomas.rpc.open_connection',
                             wraps=aiomas.rpc.open_connection) as mock:
        c = Container.create(('localhost', 5555), loop=event_loop)
        c2 = Container.create(('localhost', 5556), loop=event_loop)
        ExampleAgent(c)
        agents = [ExampleAgent(c2) for i in range(2)]
        event_loop.run_until_complete(agents[0].run('tcp://localhost:5555/0'))
        event_loop.run_until_complete(agents[1].run('tcp://localhost:5555/0'))
        c.shutdown()
        c2.shutdown()
        assert mock.call_count == 1


@pytest.mark.asyncio
async def test_connection_cache_concurrent_connects(event_loop, monkeypatch):
    """Test correct behavior for concurrent connects.

    If a second connect() call is made before the first one succeeds, a future
    should be yielded instead of trying to establish another connection.

    """
    called = event_loop.create_future()
    open_connection = aiomas.rpc.open_connection

    def open_connection_mock(*args, **kwargs):
        assert not called.done(), 'Method should only be called once!'
        called.set_result(True)
        return open_connection(*args, **kwargs)

    monkeypatch.setattr(aiomas.rpc, 'open_connection', open_connection_mock)

    c0 = await Container.create(('localhost', 5555), as_coro=True,
                                loop=event_loop)
    c1 = await Container.create(('localhost', 5556), as_coro=True,
                                loop=event_loop)
    aiomas.Agent(c1)

    async def task():
        con = await c0.connect('tcp://localhost:5556/0')
        return con

    t0 = event_loop.create_task(task())
    t1 = event_loop.create_task(task())
    r0, r1 = await asyncio.gather(t0, t1, loop=event_loop)
    assert r0._channel is r1._channel

    await c0.shutdown(as_coro=True)
    await c1.shutdown(as_coro=True)


def test_call_container_shutdown_twice(event_loop):
    c = Container.create(('localhost', 5555), loop=event_loop)
    c.shutdown()
    c.shutdown()  # Should be no problem


def test_call_container_shutdown_async(event_loop):
    c = Container.create(('localhost', 5555), loop=event_loop)
    event_loop.run_until_complete(c.shutdown(as_coro=True))


@pytest.mark.parametrize('url', [
    'http://localhost:5555/0',
    'tcp://',
    'tcp://localhost',
    'tcp://localhost:5555',
    'tcp://localhost:port',
    'tcp://localhost/0'
    'tcp://localhost:port/0',
    'ipc://',
    'ipc://[path]/',
    'ipc://path/agent',  # Forgot [] around path
])
def test_parse_url_errors(url):
    pytest.raises(ValueError, aiomas.agent.Container._parse_url, url)


def test_parse_ipv6_brackets():
    res = aiomas.agent.Container._parse_url('tcp://[::1]:5555/0')
    assert res == (('::1', 5555), '0')


@pytest.mark.parametrize(['codec', 'expected'], [
    (None, aiomas.codecs.JSON),
    (aiomas.codecs.JSON, aiomas.codecs.JSON),
    (aiomas.codecs.MsgPack, aiomas.codecs.MsgPack),
])
def test_container_arg_codec(event_loop, codec, expected):
    c = Container.create(('localhost', 5555), codec=codec, loop=event_loop)
    assert c._connect_kwargs['codec'] is expected
    c.shutdown()


def test_container_one_codec_per_connection(event_loop):
    """Make sure that every connection (RpcClient and Channel) has its own
    codec instance.

    This is important when deserializing Proxy instances.

    """
    host, port = '127.0.0.1', 5555
    c = [Container.create((host, port + i), loop=event_loop) for i in range(3)]
    [aiomas.Agent(i) for i in c]

    async def run():
        await c[0].connect('tcp://%s:%s/0' % (host, port + 1))
        await c[0].connect('tcp://%s:%s/0' % (host, port + 2))
        await c[1].connect('tcp://%s:%s/0' % (host, port + 2))

    event_loop.run_until_complete(run())

    # Test outgoing connections
    assert len(c[0]._rpc_cons) == 2
    c1, c2 = tuple(c[0]._rpc_cons)
    assert c1.channel.codec is not c2.channel.codec

    # Test incoming connections
    assert len(c[2]._rpc_cons) == 2
    c1, c2 = tuple(c[2]._rpc_cons)
    assert c1.channel.codec is not c2.channel.codec

    [i.shutdown() for i in c]


@pytest.mark.parametrize(['clock', 'expected'], [
    (None, aiomas.clocks.AsyncioClock),
    (aiomas.clocks.ExternalClock(0), aiomas.clocks.ExternalClock),
])
def test_container_arg_clock(event_loop, clock, expected):
    c = Container.create(('localhost', 5555), clock=clock, loop=event_loop)
    assert type(c._clock) is expected
    c.shutdown()


def test_agents_with_external_clock(event_loop):
    """Test the API of the clock interface from an Agent dev POV."""
    class Agent(aiomas.agent.Agent):
        result = None
        stop = False

        async def run(self):
            # Test sleeping *for* some time
            clock = self.container.clock
            start = clock.time()
            res = await clock.sleep(3, 'spam')
            assert clock.time() - start == 3
            assert res == 'spam'

            # Test doing something *at* at time
            t = clock.utcnow().replace(seconds=3)
            clock.call_at(t, self.set_result, 'spam')
            res = await clock.sleep_until(t, 'eggs')
            assert res == 'eggs'
            assert self.result == 'spam'

            # Okay, sotp it
            self.stop = True

        def set_result(self, result):
            self.result = result

        async def update_clock(self):
            clock = self.container.clock
            while not self.stop:
                await asyncio.sleep(0.01)
                clock.set_time(clock.time() + 1)

    clock = aiomas.clocks.ExternalClock(arrow.get('2015-01-01T00:00:00+00:00'),
                                        loop=event_loop)
    c = Container.create(('localhost', 5555), clock=clock, loop=event_loop)
    a = Agent(c)
    event_loop.create_task(a.run())
    event_loop.run_until_complete(a.update_clock())
    c.shutdown()


@pytest.mark.parametrize('ssl', [
    lambda certs: aiomas.agent.SSLCerts(*certs),
    lambda certs: (aiomas.util.make_ssl_server_context(*certs),
                   aiomas.util.make_ssl_client_context(*certs)),
])
def test_container_ssl(event_loop, certs, ssl):
    c = Container.create(('127.0.0.1', 5555), ssl=ssl(certs), loop=event_loop)
    agents = [ExampleAgent(c) for i in range(2)]
    event_loop.run_until_complete(agents[1].run('tcp://127.0.0.1:5555/0'))
    c.shutdown()


def test_container_ssl_arg_error(event_loop, certs):
    pytest.raises(TypeError, Container, ('127.0.0.1', 5555), ssl=tuple(certs))


def test_container_subclass(event_loop):
    """Agent should also accept subclasses of Container (issue #17)."""
    class TestContainer(Container):
        pass

    c = TestContainer.create(('localhost', 55555), loop=event_loop)
    a = Agent(c)
    assert isinstance(a, Agent)


def test_connect_timeout(event_loop):
    c1 = Container.create(('127.0.0.1', 5555), loop=event_loop)
    agent1 = ExampleAgent(c1)
    t = event_loop.create_task(agent1.run('tcp://127.0.0.1:5556/0', timeout=1))
    event_loop.run_until_complete(asyncio.sleep(.5, loop=event_loop))

    c2 = Container.create(('127.0.0.1', 5556), loop=event_loop)
    _ = ExampleAgent(c2)  # flake8: noqa

    aiomas.run(until=t, loop=event_loop)

    c1.shutdown()
    c2.shutdown()
