# Most testing for the LocalQueue transport is done in test_channel.py.
# This module only tests some corner cases that teest_channel.py does not care
# about.
import asyncio
import warnings

import pytest

import aiomas
import aiomas.local_queue as lq


def _run_repeatedly(func, repeats, expect_fail):
    """Run *func()* *repeats* times.

    If *expect_fail* is ``True``, at least one run must raise an error.
    If *expect_fail* is ``False``, no run must raise an error.

    """
    results = [func() for _ in range(repeats)]
    all_equal = all(a == b for a, b in zip(results[:-1], results[1:]))
    if all_equal and expect_fail:
        warnings.warn('This test is deterministic but should not be.',
                      UserWarning)
    elif not all_equal and not expect_fail:
        pytest.fail('this test did not produce deterministic results.')


def test_queue_registry():
    assert lq._QUEUES == {}

    q1 = lq.get_queue('test')
    assert lq._QUEUES == {'test': q1}

    q2 = lq.get_queue('test')
    assert q2 is q1
    assert lq._QUEUES == {'test': q1}

    lq.clear_queue_cache()
    assert lq._QUEUES == {}


@pytest.mark.parametrize('queue_id', [1, 'spam/eggs'])
def test_get_queue_error(queue_id):
    pytest.raises(ValueError, lq.get_queue, queue_id)
    assert lq._QUEUES == {}


@pytest.mark.parametrize('addr, fail', [
    (lq.LocalQueue('test_determinism'), False),
    (('localhost', 5555), True),
])
def test_determinism(event_loop, addr, fail):
    """Test if messages are sent/received in a deterministic order.

    There are more determinism tests for Agents and Containers in the
    test_aiomas.py module.

    """
    # The test should most likely fail if a TCP connection is used, but should
    # always pass with a LocalQueue connection
    ADDR = addr
    REPEATS = 5
    N_MSGS = 3
    N_CLIENTS = 3

    def run():
        results = []

        async def handle_client(channel):
            for _ in range(N_MSGS):
                req = await channel.recv()
                results.append(tuple(req.content))
                await req.reply(req.content[:2] + ['cya'])
            await channel.close()

        async def client(client_id):
            channel = await aiomas.channel.open_connection(ADDR,
                                                           loop=event_loop)
            for i in range(N_MSGS):
                rep = await channel.send((client_id, i, 'ohai'))
                results.append(tuple(rep))
            await channel.close()

        try:
            server = aiomas.run(
                aiomas.channel.start_server(
                    ADDR, handle_client, loop=event_loop),
                loop=event_loop,
            )
            clients = [
                event_loop.create_task(client(i))
                for i in range(N_CLIENTS)
            ]
            aiomas.run(asyncio.gather(*clients), loop=event_loop)

        finally:
            server.close()
            aiomas.run(server.wait_closed(), loop=event_loop)

        return results

    _run_repeatedly(run, REPEATS, fail)


@pytest.mark.parametrize('multi_container, fail, addr_factory', [
    # A single container should always lead to determinisic results
    (False, False, lambda i: ('localhost', 5555 + i)),
    (False, False, lambda i: aiomas.local_queue.get_queue(str(i))),
    # Multiple containers with a TCP socket should not be deterministic ...
    (True, True, lambda i: ('localhost', 5555 + i)),
    # ... but with a LocalQueue, they should be.
    (True, False, lambda i: aiomas.local_queue.get_queue(str(i))),
])
def test_container_determinisim(event_loop, multi_container, fail,
                                addr_factory):
    """Test if agents within single container produce deterministic results."""
    REPEATS = 5
    N_MSGS = 10
    N_AGENTS = 10

    def run():
        results = []

        class ServiceAgent(aiomas.Agent):
            @aiomas.expose
            def service(self, data):
                results.append(tuple(data))
                return data[:2] + ['cya']

        class TestAgent(aiomas.Agent):
            def __init__(self, aid, container):
                super().__init__(container)
                self.aid = aid

            async def run(self, remote_addr):
                remote_agent = await self.container.connect(remote_addr)
                for i in range(N_MSGS):
                    res = await remote_agent.service((self.aid, i, 'ohai'))
                    results.append(tuple(res))

        try:
            # Start one ServiceAgent (in its own container) ...
            sc = aiomas.Container.create(addr_factory(N_AGENTS),
                                         loop=event_loop)
            sa = ServiceAgent(sc)

            if multi_container:
                # ... and n TestAgents (each in its own container):
                tc = [aiomas.Container.create(addr_factory(i), loop=event_loop)
                      for i in range(N_AGENTS)]
                ta = [TestAgent(i, tc[i]) for i in range(N_AGENTS)]
            else:
                # .. and n TestAgents (in the same container)
                ta = [TestAgent(i, sc) for i in range(N_AGENTS)]

            tasks = [event_loop.create_task(a.run(sa.addr)) for a in ta]
            aiomas.run(asyncio.gather(*tasks), loop=event_loop)
        finally:
            aiomas.local_queue.clear_queue_cache()
            sc.shutdown()
            if multi_container:
                [c.shutdown() for c in tc]

        return results

    _run_repeatedly(run, REPEATS, fail)
