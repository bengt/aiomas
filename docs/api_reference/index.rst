=============
API reference
=============

The API reference provides detailed descriptions of aiomas' classes and
functions.

.. toctree::
   :maxdepth: 1

   aiomas
   aiomas.agent
   aiomas.channel
   aiomas.clocks
   aiomas.codecs
   aiomas.exceptions
   aiomas.local_queue
   aiomas.rpc
   aiomas.subproc
   aiomas.util
