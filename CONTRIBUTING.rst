How to Contribute
=================

Every open source project lives from the generous help by contributors that
sacrifice their time and *aiomas* is no different.

Here are a few guidelines to get you started:

- Try to limit each pull request to one change only.

- Run the tests before you commit.  The `docs explain`_ how to setup
  a development environment and run the tests.

- No contribution is too small; please submit as many fixes for typos and
  grammar bloopers as you can!

- Don’t break backward compatibility unless absolutely necessary.

- *Always* add tests and docs for your code.

  This is a hard rule; patches with missing tests or documentation won’t be
  merged.

- Write `good test docstrings`_.

- Obey `PEP 8`_ and `PEP 257`_.  Run ``flake8``.

Thank you for considering to contribute to *aiomas*!

.. _docs explain: https://aiomas.readthedocs.io/en/latest/development/dev_setup.html
.. _PEP 8: https://www.python.org/dev/peps/pep-0008/
.. _PEP 257: https://www.python.org/dev/peps/pep-0257/
.. _good test docstrings: https://jml.io/pages/test-docstrings.html
